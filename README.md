# Konzept Präsentation

Präsentation zur Vorstellung unseres Konzepts zur Jugendarbeit.

Die Präsentation wurde nach dem [hier beschriebenen Prinzip](https://gnulinux.ch/komplexe-systeme-aufbauend-praesentieren) erstellt.

![](https://codeberg.org/JuZeCr/konzept_praesentation/raw/branch/main/presentation.gif)

## Gesamtbild

![](https://codeberg.org/JuZeCr/konzept_praesentation/raw/branch/main/JuzeProfil.svg)

